module Turtle where

import Svg
import Color
import Utils

data Tortue = Tortue { x :: Double
                     , y :: Double
                     , angle :: Double
                     , penDown :: Bool
                     , penStyle :: SvgStyleAttributes
                     } deriving (Show)

-- types de variables
data Var = VInt Int
         | VDouble Double
         deriving (Show)

-- scope pour contenir les variables
data Scope = Scope { parentScope :: Maybe Scope, variables :: [(String, Var)] }
    deriving (Show)

scopeGetVal :: Scope -> String -> Var
scopeGetVal (Scope Nothing []) name = error $ "Variable '" <> name <> "' not found"
scopeGetVal (Scope (Just parentScope) []) name = scopeGetVal parentScope name
scopeGetVal (Scope parent ((varName,val):xs)) name
    | name == varName = val
    | otherwise = scopeGetVal (Scope parent xs) name

scopeSetVal :: Scope -> String -> Var -> Scope
scopeSetVal (Scope Nothing []) name val = error $ "Variable '" <> name <> "' not found"
scopeSetVal (Scope (Just parent) []) name val = Scope (Just (scopeSetVal parent name val)) []
scopeSetVal (Scope parent ((varName,varVal):xs)) name val
    | name == varName = Scope parent ((name, val):xs) 
    | otherwise = let (Scope newP newVal) = scopeSetVal (Scope parent xs) name val in (Scope newP ((varName, varVal):newVal))

scopeCreateVal :: Scope -> String -> Var -> Scope
scopeCreateVal (Scope parent vars) name val =
    if any (\(n, _) -> n == name) vars -- on s'assure que la variable n'existe pas déjà dans le scope courant           
        then error $ "Variable '" <> name <> "' already exists"
        else (Scope parent ((name, val):vars))

newScope :: Scope -> Scope
newScope parent = Scope (Just parent) []

closeScope :: Scope -> Scope
closeScope (Scope (Just parent) _) = parent

-- expression double
data DExpr = DAdd DExpr DExpr 
           | DMul DExpr DExpr
           | DDiv DExpr DExpr
           | DSub DExpr DExpr
           | DMod DExpr DExpr
           | DConst Double
           | DVar String
           deriving (Show)

-- expression int
data IExpr = IAdd IExpr IExpr
           | IMul IExpr IExpr
           | IDiv IExpr IExpr
           | ISub IExpr IExpr
           | IMod IExpr IExpr
           | IConst Int
           | IVar String
           deriving (Show)

-- expression color
data CExpr = Rgb IExpr IExpr IExpr
           | Hsl IExpr IExpr IExpr
           deriving (Show)

evalDExpr :: DExpr -> Scope -> Double
evalDExpr (DConst x) _ = x
evalDExpr (DVar varName) scope = case scopeGetVal scope varName of
    (VDouble v) -> v
    (VInt v)    -> fromIntegral v
evalDExpr (DAdd e1 e2) scope = evalDExpr e1 scope + evalDExpr e2 scope 
evalDExpr (DMul e1 e2) scope = evalDExpr e1 scope * evalDExpr e2 scope 
evalDExpr (DDiv e1 e2) scope = evalDExpr e1 scope / evalDExpr e2 scope 
evalDExpr (DSub e1 e2) scope = evalDExpr e1 scope - evalDExpr e2 scope

evalIExpr :: IExpr -> Scope -> Int
evalIExpr (IConst x) _ = x
evalIExpr (IVar varName) scope = case scopeGetVal scope varName of
    (VDouble v) -> floor v
    (VInt v)    -> v
evalIExpr (IAdd e1 e2) scope = evalIExpr e1 scope + evalIExpr e2 scope 
evalIExpr (IMul e1 e2) scope = evalIExpr e1 scope * evalIExpr e2 scope 
evalIExpr (IDiv e1 e2) scope = evalIExpr e1 scope `div` evalIExpr e2 scope 
evalIExpr (ISub e1 e2) scope = evalIExpr e1 scope - evalIExpr e2 scope
evalIExpr (IMod e1 e2) scope = evalIExpr e1 scope `mod` evalIExpr e2 scope

evalCExpr :: CExpr -> Scope -> Color
evalCExpr (Rgb re ge be) scope = rgbColor (evalIExpr re scope) (evalIExpr ge scope) (evalIExpr be scope)
evalCExpr (Hsl he se le) scope = hslColor (evalIExpr he scope) (evalIExpr se scope) (evalIExpr le scope)

data Command = Avance DExpr 
             | Tourne DExpr 
             | BaisseCrayon 
             | LeveCrayon
             | SetFillColor CExpr
             | SetStrokeColor CExpr
             | SetStrokeWidth DExpr
             | Repeat IExpr [Command]
             | DAffect String DExpr
             | DDeclare String DExpr
             | IAffect String IExpr
             | IDeclare String IExpr
    deriving (Show)

data Env = Env
    { turtle :: Tortue
    , svg :: Svg
    , scope :: Scope
    }

initialEnv = Env (Tortue 0 0 0 True defaultStyle) (Svg []) (Scope Nothing [])

execProgramme :: [Command] -> Svg
execProgramme commands = svg $ execCommands initialEnv commands

execCommands :: Env -> [Command] -> Env
execCommands env commands = foldl execCommand env commands

execCommand :: Env -> Command -> Env
-- Styles
execCommand (Env (Tortue x y angle penDown penStyle) svg scope) (SetStrokeWidth widthExpr) = (Env (Tortue x y angle penDown penStyle { strokeWidth = evalDExpr widthExpr scope}) svg scope)
execCommand (Env (Tortue x y angle penDown penStyle) svg scope) (SetStrokeColor colorExpr) = (Env (Tortue x y angle penDown penStyle { stroke = evalCExpr colorExpr scope }) svg scope)
execCommand (Env (Tortue x y angle penDown penStyle) svg scope) (SetFillColor colorExpr) = (Env (Tortue x y angle penDown penStyle { fill = evalCExpr colorExpr scope }) svg scope)

-- Pen
execCommand (Env (Tortue x y angle penDown penStyle) svg scope) BaisseCrayon = Env (Tortue x y angle True penStyle) svg scope
execCommand (Env (Tortue x y angle penDown penStyle) svg scope) LeveCrayon = Env (Tortue x y angle False penStyle) svg scope

-- Mouvement
execCommand (Env (Tortue x y angle penDown penStyle) svg scope) (Tourne addAngleExpr) = Env (Tortue x y newAngle penDown penStyle) svg scope
    where newAngle = angle + (evalDExpr addAngleExpr scope)

execCommand (Env (Tortue x y angle penDown penStyle) (Svg shapes) scope) (Avance depExpr)  = (Env newTortue newSvg scope)
    where dep = evalDExpr depExpr scope
          newX = x + (cos (degToRad angle)) * dep
          newY = y + (sin (degToRad angle)) * dep
          newTortue = (Tortue newX newY angle penDown penStyle)
          newSvg = if penDown then Svg ((Line x y newX newY penStyle):shapes) else (Svg shapes)

-- Misc
execCommand initialEnv (Repeat nExpr commands) = iterate applyCommands initialEnv !! (evalIExpr nExpr (scope initialEnv))
    where applyCommands = \env -> execCommands env commands

-- Variables
execCommand (Env tortue svg scope) (DAffect name valExpr) = Env tortue svg (scopeSetVal scope name (VDouble (evalDExpr valExpr scope)))
execCommand (Env tortue svg scope) (DDeclare name valExpr) = Env tortue svg (scopeCreateVal scope name (VDouble (evalDExpr valExpr scope)))
execCommand (Env tortue svg scope) (IAffect name valExpr) = Env tortue svg (scopeSetVal scope name (VInt (evalIExpr valExpr scope)))
execCommand (Env tortue svg scope) (IDeclare name valExpr) = Env tortue svg (scopeCreateVal scope name (VInt (evalIExpr valExpr scope)))