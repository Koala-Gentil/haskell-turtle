module Utils where

degToRad :: Double -> Double
degToRad deg = deg * pi / 180

inBounds :: Ord a => a -> a -> a -> Bool
inBounds mini maxi x = x >= mini && x <= maxi

maybeToEither :: a -> Maybe b -> Either a b
maybeToEither x Nothing = Left x
maybeToEither _ (Just x) = Right x
