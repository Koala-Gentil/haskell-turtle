module Color
  ( Color
  , rgbColor
  , hslColor
  , hslaColor
  , colorToCss
  , randomHueColorHSLA
  , noneColor
  , blackColor
  , whiteColor
  , greyColor
  , redColor
  , blueColor
  , greenColor
  ) where

import           Control.Exception (assert)
import           System.Random     (randomRIO)

data Color
  = None
  | ColorRGB
      { red   :: Int
      , green :: Int
      , blue  :: Int
      }
  | ColorHSL
      { hue        :: Int
      , saturation :: Int
      , lightness  :: Int
      }
  | ColorHSLA
      { hue        :: Int
      , saturation :: Int
      , lightness  :: Int
      , alpha      :: Int
      }
  deriving (Show, Eq)

colorToCss :: Color -> String
colorToCss None = "none"
colorToCss (ColorRGB r g b) =
  "rgb(" <> (show r) <> ", " <> (show g) <> ", " <> (show b) <> ")"
colorToCss (ColorHSL h s l) =
  "hsl(" <> (show h) <> ", " <> (show s) <> "%, " <> (show l) <> "%)"
colorToCss (ColorHSLA h s l a) = 
  "hsla(" <> (show h) <> ", " <> (show s) <> "%, " <> (show l) <> "%, " <> (show a) <> "%)"

randomHueColorHSLA :: Int -> Int -> Int -> IO Color
randomHueColorHSLA s l a = do
  randomHue <- randomRIO (0, 255)
  return (hslaColor randomHue s l a)

-- | smart constructor for ColorRGB
rgbColor :: Int -> Int -> Int -> Color
rgbColor r g b
  | all (inBounds 0 255) [r, g, b] = ColorRGB r g b
  | otherwise = error "Invalid rgbColor"

-- | smart constructor for ColorHSL
hslColor :: Int -> Int -> Int -> Color
hslColor h s l
  | inBounds 0 360 h && inBounds 0 100 s && inBounds 0 100 l = ColorHSL h s l
  | otherwise = error "Invalid hslColor"
  

-- | smart constructor for ColorHSLA
hslaColor :: Int -> Int -> Int -> Int -> Color
hslaColor h s l a
  | inBounds 0 360 h && inBounds 0 100 s && inBounds 0 100 l && inBounds 0 100 a = ColorHSLA h s l a
  | otherwise = error "Invalid hslaColor"

inBounds :: Ord a => a -> a -> a -> Bool
inBounds mini maxi x = x >= mini && x <= maxi

-- some colors
noneColor = None

blackColor = ColorRGB 0 0 0

whiteColor = ColorRGB 255 255 255

greyColor = ColorRGB 128 128 128

redColor = ColorRGB 255 0 0

greenColor = ColorRGB 0 255 0

blueColor = ColorRGB 0 0 255
