module Xml where

-- implémentation non complète de Xml mais suffit pour Svg :)

type Attribute = (String, String)

data XmlTag = 
  XmlTag
    { elt        :: String
    , attributes :: [Attribute]
    , children   :: [XmlTag]
    }
  deriving (Show, Eq)

class XmlConvertible a where
  convertXml :: a -> XmlTag

instance XmlConvertible XmlTag where
  convertXml = id

exportXml :: XmlConvertible a => a -> String
exportXml e = "<?xml version=\"1.0\"?>" ++ (exportXmlTag . convertXml) e

exportXmlTag :: XmlTag -> String
exportXmlTag (XmlTag elt attributes children) =
  "<" ++
  elt ++
  sattr ++
  if null children
    then "/>"
    else ">" ++ schildren ++ "</" ++ elt ++ ">"
  where
    sattr =
      concat $
      map
        (\(key, value) -> " " ++ key ++ "=" ++ "\"" ++ value ++ "\"")
        attributes
    schildren = concat $ map exportXmlTag children
