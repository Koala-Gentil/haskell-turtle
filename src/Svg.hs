module Svg where

import           Color
import           Xml

data SvgStyleAttributes =
  SvgStyleAttributes
    { fill        :: Color
    , stroke      :: Color
    , strokeWidth :: Double
    }
  deriving (Show, Eq)

data SvgShape
  = Rectangle
      { x      :: Double
      , y      :: Double
      , width  :: Double
      , height :: Double
      , style  :: SvgStyleAttributes
      }
  | Line
      { x1    :: Double
      , y1    :: Double
      , x2    :: Double
      , y2    :: Double
      , style :: SvgStyleAttributes
      }
  | Circle
      { cx    :: Double
      , cy    :: Double
      , r     :: Double
      , style :: SvgStyleAttributes
      }
  | Path
      { coords :: [(Double, Double)]
      , style  :: SvgStyleAttributes
      }
  deriving (Show, Eq)

data BBox =
  BBox
    { left   :: Double
    , bottom :: Double
    , right  :: Double
    , top    :: Double
    }
  deriving (Show, Eq)


class Boxable a where
  getBBox :: a -> BBox

instance Boxable SvgShape where
  getBBox (Rectangle x y width height _) = BBox x (y + height) (x + width) y
  getBBox (Line x1 y1 x2 y2 _) = BBox (min x1 x2) (max y1 y2) (max x1 x2) (min y1 y2)
  getBBox (Circle cx cy r _) = BBox (cx - r) (cy + r) (cx + r) (cy - r)
  getBBox (Path coords _) = BBox minX maxY maxX minY
    where
      maxX = maximum $ map fst coords
      minX = minimum $ map fst coords
      maxY = maximum $ map snd coords
      minY = minimum $ map snd coords

instance Boxable a => Boxable [a] where
  getBBox boxables = BBox minLeft maxBottom maxRight minTop 
    where
      bboxes = map getBBox boxables
      minLeft = minimum $ map left bboxes
      maxBottom = maximum $ map bottom bboxes
      minTop = minimum $ map top bboxes
      maxRight = maximum $ map right bboxes

data Svg =
  Svg
    { shapes :: [SvgShape]
    }

instance Boxable Svg where
  getBBox (Svg shapes) = getBBox shapes

defaultStyle :: SvgStyleAttributes
defaultStyle =
  SvgStyleAttributes {fill = noneColor, stroke = blackColor, strokeWidth = 1}

styleXmlAttributes :: SvgStyleAttributes -> [Attribute]
styleXmlAttributes (SvgStyleAttributes fill stroke strokeWidth) =
  [ ("fill", colorToCss fill)
  , ("stroke", colorToCss stroke)
  , ("stroke-width", show strokeWidth)
  ]

instance XmlConvertible SvgShape where
  convertXml (Rectangle x y width height style) =
    XmlTag
      "rect"
      ([ ("x", show x)
       , ("y", show y)
       , ("width", show width)
       , ("height", show height)
       ] ++
       styleXmlAttributes style)
      []
  convertXml (Circle cx cy r style) =
    XmlTag
      "circle"
      ([("cx", show cx), ("cy", show cy), ("r", show r)] ++
       styleXmlAttributes style)
      []
  convertXml (Line x1 y1 x2 y2 style) =
    XmlTag
      "line"
      ([("x1", show x1), ("y1", show y1), ("x2", show x2), ("y2", show y2)] ++
       styleXmlAttributes style)
      []
  convertXml (Path coords style) =
    XmlTag "line" ([("d", getPathData coords)] ++ styleXmlAttributes style) []

instance XmlConvertible Svg where
  convertXml (Svg shapes) =
    XmlTag
      "svg"
      [("width", "100%"), ("height", "100vh"), ("xmlns", "http://www.w3.org/2000/svg"), ("viewBox", viewBox)]
      (map convertXml shapes)
    where
      bbox = getBBox (Svg shapes)
      viewBox = (show $ left bbox) <> " " <> (show $ top bbox) <> " " <> (show ((right bbox) - (left bbox))) <> " " <> (show ((bottom bbox) - (top bbox)))

getPathData :: [(Double, Double)] -> String
getPathData [] = ""
getPathData ((x, y):coords) =
  "M " ++
  (show x) ++
  " " ++
  (show y) ++
  " " ++
  concat (map (\(x, y) -> "L " ++ (show x) ++ " " ++ (show y) ++ " ") coords)


example = Svg [Rectangle (-100) (-100) 200 300 defaultStyle]