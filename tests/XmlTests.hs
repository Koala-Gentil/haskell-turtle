import Test.Tasty
import Test.Tasty.HUnit

import Xml

data Cat = Cat { name :: String, color :: String }

data Cats = Cats [Cat]

instance XmlConvertible Cat where
    convertXml (Cat name color) = XmlTag "cat" [("name", name), ("color", color)] []

instance XmlConvertible Cats where
    convertXml (Cats cats) = XmlTag "cats" [] (map convertXml cats)

catsExample = Cats [ (Cat "kitty" "white"), (Cat "ficelle" "black") ]

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" 
    [ testCase "exportXmlTag without attributes nor children" $
        exportXmlTag XmlTag {
            elt = "foo",
            attributes = [],
            children = []
        } @?= "<foo/>"

    , testCase "exportXmlTag with attributes" $
        exportXmlTag XmlTag {
            elt = "foo",
            attributes = [("bar", "baz"), ("titi", "toto")],
            children = []
        } @?= "<foo bar=\"baz\" titi=\"toto\"/>"

    , testCase "exportXmlTag with attributes and children" $
        exportXmlTag XmlTag {
            elt = "foo",
            attributes = [("bar", "baz"), ("titi", "toto")],
            children = 
            [
                XmlTag "fuz" [] []
            ]
        } @?= "<foo bar=\"baz\" titi=\"toto\"><fuz/></foo>"
    
    , testCase "exportXml catsExample" $
        exportXml catsExample @?= "<?xml version=\"1.0\"?><cats><cat name=\"kitty\" color=\"white\"/><cat name=\"ficelle\" color=\"black\"/></cats>"
    ]
