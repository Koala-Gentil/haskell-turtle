module Main where

import           Color
import           Control.Monad      (replicateM)
import           Svg
import           System.Environment
import           System.Random
import           Text.Read
import           Xml
import           Utils

-- constants
maxX = 1000
minX = -1000
maxY = 1000
minY = -1000
minSize = 10
maxSize = 150

randomSquare :: IO SvgShape
randomSquare = do
  size <- randomRIO (minSize, maxSize)
  x <- randomRIO (minX, maxX)
  y <- randomRIO (minY, maxY)
  color <- randomHueColorHSLA 50 50 80
  return
    (Rectangle
       { x = x
       , y = y
       , width = size
       , height = size
       , style = defaultStyle {fill = color}
       })

readArgs :: IO (Either String (Int, String))
readArgs = do
  args <- getArgs
  case args of
    [nbSquares, filePath] -> return $ (,) <$> maybeToEither "Entier attendu" (readMaybe nbSquares) <*> pure filePath
    _                     -> return $ Left "2 arguments attendu"

main = do
  args <- readArgs
  case args of
    Right (nbSquares, filePath) -> do
      squares <- replicateM nbSquares randomSquare
      writeFile filePath (exportXml (Svg squares))
    Left err -> do
      putStrLn $ "Erreur: " <> err
      prog <- getProgName
      putStrLn $ "Utilisation: " <> prog <> " 50 output/random_squares.svg"
