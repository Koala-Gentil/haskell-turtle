module Main where

import           Turtle
import           Xml
import           Color

carre size = [ Repeat (IConst 4) [Avance (DConst size), Tourne (DConst 90)] ]

moulin = [Repeat (IConst 4) [ (Avance (DConst 100))
                      , (Tourne (DConst 90))
                      , (Avance (DConst 50))
                      , (Tourne (DConst 90))
                      , (Avance (DConst 50))
                      , (Tourne (DConst 90))
                      , (Avance (DConst 50))
                      , (Tourne (DConst (-90)))
                      , (Avance (DConst 50))
                      , (Tourne (DConst 90))
                      ]]

regularPolygon :: Int -> Double -> [Command]
regularPolygon n radius =
  let angle = 360.0 / (fromIntegral n)
      size = sin (2 * pi / (fromIntegral n)) * radius -- merci wikipédia
    in [Repeat (IConst n) [(Avance (DConst size)), (Tourne (DConst angle))]]

koch :: [Command] -> [Command]
koch [] = []
koch ((Repeat n rep_commands):commands) =
  (Repeat n (koch rep_commands)) : (koch commands)
koch ((Avance c):commands) =
  let newC = DDiv c (DConst 3) -- ça marche :O
   in (Avance newC) :
      (Tourne (DConst (-60))) :
      (Avance newC) :
      (Tourne (DConst (-240))) :
      (Avance newC) : (Tourne (DConst (-60))) : (Avance newC) : (koch commands)
koch (any:commands) = any : (koch commands)

-- applique n itération de koch
kochIt initialShape n = iterate koch initialShape !! n

triangle = [Repeat (IConst 3) [(Avance (DConst 300)), (Tourne (DConst 120))]]

flocon n = kochIt triangle n

spiraleCarre = 
  [ (DDeclare "a" (DConst 20))
  , (SetStrokeColor (Rgb (IConst 54) (IConst 204) (IConst 224)))
  , (SetStrokeWidth (DConst 5))
  , (Repeat
       (IConst 100)
       [ Avance (DVar "a")
       , Tourne (DConst 90)
       , DAffect "a" (DAdd (DVar "a") (DConst 10))
       ])
  ]

artContemporain =
  [ (DDeclare "a" (DConst 100))
  , (Repeat
       (IConst 100)
       [ Avance (DVar "a")
       , Tourne (DVar "a")
       , DAffect "a" (DMul (DVar "a") (DConst 1.01))
       ])
  ]

rainbowSpirale = 
  [ (DDeclare "size" (DConst 10))
  , (IDeclare "hue" (IConst 0))
  , (SetStrokeWidth (DConst 20))
  , (Repeat
       (IConst 720)
       [ SetStrokeColor (Hsl (IVar "hue") (IConst 70) (IConst 70))
       , Avance (DVar "size")
       , Tourne (DConst 10)
       , DAffect "size" (DAdd (DVar "size") (DConst 1))
       , IAffect "hue"  (IMod (IAdd (IVar "hue") (IConst 1)) (IConst 360))
       ])
  ]

main = do
  writeFile "output/carre.svg" $ exportXml $ execProgramme (carre 500)
  writeFile "output/moulin.svg" $ exportXml $ execProgramme moulin
  writeFile "output/octogone.svg" $ exportXml $ execProgramme (regularPolygon 8 500)
  writeFile "output/circle.svg" $ exportXml $ execProgramme (regularPolygon 360 500)
  writeFile "output/flocon.svg" $ exportXml $ execProgramme (flocon 5)
  writeFile "output/moulin_koch.svg" $ exportXml $ execProgramme (kochIt moulin 5)
  writeFile "output/spirale_carre.svg" $ exportXml $ execProgramme spiraleCarre
  writeFile "output/art_contemporain.svg" $ exportXml $ execProgramme artContemporain
  writeFile "output/rainbow_spirale.svg" $ exportXml $ execProgramme rainbowSpirale
  writeFile "output/rainbow_spirale_koch.svg" $ exportXml $ execProgramme (kochIt rainbowSpirale 3)

