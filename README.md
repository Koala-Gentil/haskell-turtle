# HASKELL-TURTLE

## Auteur

- Hugo Doyer

## Utilisations

J'ai utilisé cabal pour créer le projet et gérer les 2 exécutables réalisés. Pour lancer les éxecutables et tests :

```bash
cabal new-run turtle # créer un ensemble de svg dans output
cabal new-run random-squares 500 output/random_squares.svg 

cabal new-test # lance les tests unitaires
```

## Informations sur les différents fichiers

- `exe`
    - `Main.hs` executable pour créer différents svg
    - `RandomSquares.hs` executable avec lecture d'arguments pour créer _n_ carrés aléatoirement.
- `src`
    - `Utils.hs` fonctions diverses
    - `Xml.hs` implémentation non complète mais suffisante pour représenter des balises Xml. Créer un TDA XmlTag et une classe XmlConvertible requérant une fonction `convertXml`
    - `Svg.hs` créer le TDA Svg avec les formes primitives instanciant la classe `XmlConvertible`. J'ai aussi créé une classe `Boxable` pour récupérer la BoundingBox des différents éléments afin d'ajuster automatiquement la viewbox du Svg lors de son exportation. De cette façon le dessin est toujours centré et zoomé.
    - `Color.hs` TDA pour représenter les couleurs, j'ai aussi créer des "smartConstructors" associés au TDA.
    - `Turtle.hs` Module pour gérer les expressions (`IExpr` pour les entiers et `DExpr` pour les double), le `Scope`, la `Tortue`, l'`Environement` ainsi que les `Command`.
- `tests`
    - `XmlTests.hs` Tests unitaires pour le module Xml avec les librairies `Tasty` et `HUnit`.
- `output` Mes résultats !

En ce qui concerne l'ajout des variables, j'ai ajouté la notion de scope parent, mais je n'ai pas fait l'ajout de fonctions (désolé :c) donc il n'y a jamais de nouveau scope qui se créé. J'ai aussi du séparer la notion d'expression du résultat entier et double. J'ai aussi ajouté la possiblité de créer des couleurs à partir d'expressions d'entiers.

Example :
```haskell
rainbowSpirale = 
  [ (DDeclare "size" (DConst 10))
  , (IDeclare "hue" (IConst 0))
  , (SetStrokeWidth (DConst 20))
  , (Repeat
       (IConst 720)
       [ SetStrokeColor (Hsl (IVar "hue") (IConst 70) (IConst 70)) -- Yeah !
       , Avance (DVar "size")
       , Tourne (DConst 10)
       , DAffect "size" (DAdd (DVar "size") (DConst 1))
       , IAffect "hue"  (IMod (IAdd (IVar "hue") (IConst 1)) (IConst 360))
       ])
  ]
```

Les réponses aux différentes questions pour créer des formes sont codées sous la forme de fonctions dans le fichier `exe/Main.hs`. L'ajout des variables et des expressions m'a par contre obligé à refactoriser, rendant l'expression en `[Command]` laborieuse.

Un autre fichier `exe/RandomSquares.hs` pour la question __1.2__


PS: Ne pas oublier de regarder mon superbe `rainbow_spirale_koch.svg` !